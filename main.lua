require 'includes'

-- Variables
tileMap = {w = 1, h = 1, tiles = {}}

function fillMap(x, y, val)
    tileMap.tiles[x][y] = val
end

function love.load()
    tileMap.w = 80
    tileMap.h = 24
    f = ROT.Display(tileMap.w, tileMap.h)

    for i = 1, tileMap.w do
        tileMap.tiles[i] = {}
    end

    arena = ROT.Map.Arena:new(tileMap.w, tileMap.h)
    arena:create(fillMap)
end

function love.update()
    f:clear()
    for x=1, tileMap.w do
        for y=1, tileMap.h do
            local char = tileMap.tiles[x][y] == 0 and '.' or tileMap.tiles[x][y] == 1 and '#' 
            f:write(char, x, y)
        end
    end
end

function love.draw()
    f:draw()
end